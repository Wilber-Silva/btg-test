import { IAuthToken } from '@Interfaces/Auth';

declare global {
  namespace Express {
    interface User extends IAuthToken {}
  }
}
