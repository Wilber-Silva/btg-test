import { MigrationInterface, QueryRunner } from 'typeorm';

export class createPaymentOrder1648315815439 implements MigrationInterface {
  name = 'createPaymentOrder1648315815439';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TYPE "payment_order_status_enum" AS ENUM('CREATED', 'APPROVED', 'SCHEDULED', 'REJECTED')`,
    );
    await queryRunner.query(
      `CREATE TABLE "payment_order" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "externalId" uuid NOT NULL, "amount" float NOT NULL, "expectedOn" date NOT NULL, "dueDate" date, "transferDate" date, "status" "payment_order_status_enum" NOT NULL, "operation" character varying(100) NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP, CONSTRAINT "PK_f5221735ace059250daac9d9803" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_40b6989982655e940c928373ab" ON "payment_order" ("externalId") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_f736844904bde3cb28b3236875" ON "payment_order" ("status") `,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP INDEX "IDX_f736844904bde3cb28b3236875"`);
    await queryRunner.query(`DROP INDEX "IDX_40b6989982655e940c928373ab"`);
    await queryRunner.query(`DROP TABLE "payment_order"`);
    await queryRunner.query(`DROP TYPE "payment_order_status_enum"`);
  }
}
