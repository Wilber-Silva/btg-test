import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { Repositories } from './repository.instances';

import typeormDefaultConfig from './Configs/typeorm-default.config';

@Module({
  imports: [
    ConfigModule.forRoot({
      cache: true,
      isGlobal: true,
    }),
    TypeOrmModule.forRoot(typeormDefaultConfig),
    TypeOrmModule.forFeature(Repositories),
  ],
  exports: [TypeOrmModule.forFeature(Repositories)],
})
export class DatabaseModule {}
