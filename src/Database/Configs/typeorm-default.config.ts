import { TypeOrmModuleOptions } from '@nestjs/typeorm';

import * as databaseEntities from './../Entities';

const entities: any = (
  Object.keys(databaseEntities) as Array<keyof typeof databaseEntities>
).map((entity: keyof typeof databaseEntities) => databaseEntities[entity]);

const ormConfig: TypeOrmModuleOptions = {
  name: 'default',
  type: 'postgres',
  host: process.env.POSTGRESQL_HOST,
  port: Number(process.env.POSTGRESQL_PORT),
  username: process.env.POSTGRESQL_USERNAME,
  password: process.env.POSTGRESQL_PASSWORD,
  database: process.env.POSTGRESQL_DATABASE,
  logging:
    typeof process.env.DEBUG === 'string'
      ? process.env.DEBUG === 'true'
      : process.env.DEBUG,
  entities,
  synchronize: false,
  migrationsRun: false,
  migrations: [__dirname + '/../Migrations/**{.ts,.js}'],
  cli: {
    migrationsDir: 'src/Database/Migrations',
    entitiesDir: 'src/Database/Entities',
  },
};

export default ormConfig;
