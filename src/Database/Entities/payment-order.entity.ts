import { PaymentOrderStatus } from '@Enum';
import { IPaymentOrderModel } from '@Protocols/Model';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Index,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('payment_order')
export class PaymentOrderEntity implements IPaymentOrderModel {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Index()
  @Column({ type: 'uuid' })
  externalId: string;

  @Column({ type: 'float' })
  amount: number;

  @Column({ type: 'date' })
  expectedOn: string | Date;

  @Column({ type: 'date', nullable: true })
  dueDate?: string | Date;

  @Column({ type: 'date', nullable: true })
  transferDate?: string | Date;

  @Index()
  @Column({ type: 'enum', enum: PaymentOrderStatus })
  status: PaymentOrderStatus;

  @Column({ type: 'varchar', length: 100, nullable: false })
  operation: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date | null;
}
