import { IPaymentOrderModel } from '@Protocols/Model';
import { IUseCase } from '@Protocols/Usecase/usecase.interface';

export interface IGetOnePaymentOrderUseCase
  extends IUseCase<string, IPaymentOrderModel> {}
