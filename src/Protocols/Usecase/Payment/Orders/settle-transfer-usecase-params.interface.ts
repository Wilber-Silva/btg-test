import { PaymentOrderOperation } from '@Enum';
import { IHttpRequestPaymentOrdersSettleTransfer } from '@Protocols/HttpRequest';

export interface ISettleTheTransferUsecaseParams {
  operation: PaymentOrderOperation;
  paymentOrdersSettleTransfer: IHttpRequestPaymentOrdersSettleTransfer;
}
