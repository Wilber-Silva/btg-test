import { PaymentOrderStatus } from '@Enum';

export interface ISettleTheTransferUsecaseResults {
  status: PaymentOrderStatus;
  id: string;
}
