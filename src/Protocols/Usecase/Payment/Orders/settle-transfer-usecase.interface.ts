import { IUseCase } from './../../usecase.interface';
import { ISettleTheTransferUsecaseParams } from './settle-transfer-usecase-params.interface';
import { ISettleTheTransferUsecaseResults } from './settle-transfer-usecase-results.interface';
export interface ISettleTheTransferUsecase
  extends IUseCase<
    ISettleTheTransferUsecaseParams,
    ISettleTheTransferUsecaseResults
  > {}
