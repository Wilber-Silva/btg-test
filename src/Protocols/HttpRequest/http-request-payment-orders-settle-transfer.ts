export interface IHttpRequestPaymentOrdersSettleTransfer {
  externalId: string;
  amount: number;
  expectedOn: Date | string;
  dueDate?: Date | string;
}
