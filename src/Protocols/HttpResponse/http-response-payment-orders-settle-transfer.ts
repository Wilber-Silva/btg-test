import { PaymentOrderStatus } from '@Enum/payment-order-status';

export interface IHttpResponsePaymentOrdersSettleTransfer {
  internalId: string;
  status: PaymentOrderStatus;
}
