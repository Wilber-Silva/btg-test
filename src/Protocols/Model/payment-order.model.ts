import { IPaymentOrderSchema } from '@Protocols/Schema';
import { IBaseModel } from './base.model';

export interface IPaymentOrderModel extends IBaseModel, IPaymentOrderSchema {}
