import { IBaseModel } from './base.model';
import { IUserSchema } from './../Schema';
export interface IUserModel extends IBaseModel, IUserSchema {}
