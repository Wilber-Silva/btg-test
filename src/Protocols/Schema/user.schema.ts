export interface IUserSchema {
  username?: string;
  firstName?: string;
  lastName?: string;
  nickname?: string;
}
