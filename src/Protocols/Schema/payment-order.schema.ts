import { PaymentOrderStatus } from '@Enum/payment-order-status';

export interface IPaymentOrderSchema {
  externalId: string;
  amount: number;
  expectedOn: string | Date;
  dueDate?: string | Date;
  transferDate?: string | Date;
  status: PaymentOrderStatus;
  operation: string;
}
