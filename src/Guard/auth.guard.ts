import { UnauthorizedException } from '@Exceptions/unauthorized.exception';
import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    const headers = request.headers;
    const authorization = headers['x-api-key'] || headers['X-API-KEY'];
    const keys = process.env.APP_KEYS;

    if (!keys?.includes(authorization)) {
      throw new UnauthorizedException({
        message: 'Invalid token',
        stack: 'AuthGuard',
      });
    }

    request.user = authorization;

    return authorization;
  }
}
