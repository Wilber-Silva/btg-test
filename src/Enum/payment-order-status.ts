export enum PaymentOrderStatus {
  CREATED = 'CREATED',
  APPROVED = 'APPROVED',
  SCHEDULED = 'SCHEDULED',
  REJECTED = 'REJECTED',
}
