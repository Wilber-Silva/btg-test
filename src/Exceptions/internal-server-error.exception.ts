import { IException } from '@Protocols/exception.interface';
import { HttpStatus } from '@nestjs/common';
import { BaseException } from './base.exception';

export class InternalServerErrorException extends BaseException {
  constructor(params: IException) {
    params.statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
    super(params);
  }
}
