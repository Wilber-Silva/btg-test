import { IException } from '@Protocols/exception.interface';
import { HttpStatus } from '@nestjs/common';
import { BaseException } from './base.exception';

export class ConflictException extends BaseException {
  constructor(params: IException) {
    params.statusCode = HttpStatus.CONFLICT;
    super(params);
  }
}
