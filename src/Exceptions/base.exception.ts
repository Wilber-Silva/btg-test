import { IError, IException } from '@Protocols/exception.interface';
import { HttpException, HttpStatus } from '@nestjs/common';

export class BaseException extends HttpException implements IException {
  message: string;
  errors?: IError[];
  statusCode?: number;
  constructor(params: IException) {
    const response = {
      message: params.message,
      errors: params.errors,
    };
    if (!params.statusCode) {
      params.statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
    }
    super(response, params.statusCode);

    this.statusCode = params.statusCode;
    this.message = params.message;
    this.errors = params.errors;
    this.stack = params.stack;

    console.debug({
      message: this.message,
      stack: this.stack,
    });
  }
}
