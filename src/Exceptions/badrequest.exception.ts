import { IException } from '@Protocols/exception.interface';
import { HttpStatus } from '@nestjs/common';
import { BaseException } from './base.exception';

export class BadRequestException extends BaseException {
  constructor(params: IException) {
    params.statusCode = HttpStatus.BAD_REQUEST;
    super(params);
  }
}
