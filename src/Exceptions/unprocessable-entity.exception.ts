import { IException } from '@Protocols/exception.interface';
import { HttpStatus } from '@nestjs/common';
import { BaseException } from './base.exception';

export class UnprocessableEntityException extends BaseException {
  constructor(params: IException) {
    params.statusCode = HttpStatus.UNPROCESSABLE_ENTITY;
    super(params);
  }
}
