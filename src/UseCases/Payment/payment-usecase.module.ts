import { Module } from '@nestjs/common';
import { PaymentOrderUsecaseModule } from './Orders/order-usecase.module';
@Module({
  imports: [PaymentOrderUsecaseModule],
  exports: [PaymentOrderUsecaseModule],
})
export class PaymentUsecaseModule {}
