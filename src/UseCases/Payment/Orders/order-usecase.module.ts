import { Module } from '@nestjs/common';

import { HttpSettleClientModule } from '@Providers/HttpClient/http-settle-client.module';
import { GetOnePaymentOrderUsecase } from './usecases/get-one-payment-order.usecase';
import { SettleTheTransferUsecase } from './usecases/settle-transfer.usecase';
@Module({
  imports: [HttpSettleClientModule],
  providers: [SettleTheTransferUsecase, GetOnePaymentOrderUsecase],
})
export class PaymentOrderUsecaseModule {}
