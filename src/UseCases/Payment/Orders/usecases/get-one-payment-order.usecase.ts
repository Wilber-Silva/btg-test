import { PaymentOrderRepository } from '@Database/Repositories';
import { NotfoundException } from '@Exceptions/notfound-exception';
import { IPaymentOrderModel } from '@Protocols/Model';
import { IGetOnePaymentOrderUseCase } from '@Protocols/Usecase';
import { getCustomRepository } from 'typeorm';

export class GetOnePaymentOrderUsecase implements IGetOnePaymentOrderUseCase {
  private paymentOrderRepository: PaymentOrderRepository;
  private async setPaymentOrderRepository() {
    if (!this.paymentOrderRepository)
      this.paymentOrderRepository = getCustomRepository(PaymentOrderRepository);
  }

  async execute(id: string): Promise<IPaymentOrderModel> {
    await this.setPaymentOrderRepository();
    const paymentOrder = await this.paymentOrderRepository.findOne({
      where: { id },
    });

    if (!paymentOrder)
      throw new NotfoundException({ message: 'Payment order not found' });

    return paymentOrder;
  }
}
