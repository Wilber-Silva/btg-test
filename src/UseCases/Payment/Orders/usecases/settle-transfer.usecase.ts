import { PaymentOrderRepository } from '@Database/Repositories';
import { PaymentOrderStatus } from '@Enum/payment-order-status';
import { InternalServerErrorException } from '@Exceptions/internal-server-error.exception';
import { Inject } from '@nestjs/common';
// import of possible exception import { ConflictException } from "@Exceptions/conflict.exception";
import {
  ISettleTheTransferUsecase,
  ISettleTheTransferUsecaseResults,
} from '@Protocols/Usecase';
import { ISettleTheTransferUsecaseParams } from '@Protocols/Usecase/Payment/Orders/settle-transfer-usecase-params.interface';
import { HttpSettleClientService } from '@Providers/HttpClient/http-settle-client.service';
import { getCustomRepository, In } from 'typeorm';

export class SettleTheTransferUsecase implements ISettleTheTransferUsecase {
  private paymentOrderRepository: PaymentOrderRepository;
  constructor(
    @Inject(HttpSettleClientService)
    private readonly httpSettleClient: HttpSettleClientService,
  ) {}

  private async setPaymentOrderRepository() {
    if (!this.paymentOrderRepository)
      this.paymentOrderRepository = getCustomRepository(PaymentOrderRepository);
  }

  async execute({
    operation,
    paymentOrdersSettleTransfer,
  }: ISettleTheTransferUsecaseParams): Promise<ISettleTheTransferUsecaseResults> {
    await this.setPaymentOrderRepository();
    const oldTransfer = await this.paymentOrderRepository.findOne({
      where: {
        operation,
        status: In([PaymentOrderStatus.SCHEDULED, PaymentOrderStatus.CREATED]),
      },
    });

    let status: PaymentOrderStatus = PaymentOrderStatus.CREATED;

    if (oldTransfer) {
      status = PaymentOrderStatus.REJECTED;
      // possible throw throw new ConflictException({ message: `Founded another ${operation} operation pending` })
    }

    try {
      const resultStatus = await this.httpSettleClient.sendSettle({
        operation,
        status,
        ...paymentOrdersSettleTransfer,
      });
      if (resultStatus && status !== resultStatus) {
        status = resultStatus;
      }
    } catch (error) {
      throw new InternalServerErrorException({
        message: 'Error interno no serviço de transferência',
      });
    }

    const createdPaymentOrder = await this.paymentOrderRepository.save({
      operation,
      status,
      ...paymentOrdersSettleTransfer,
    });

    return {
      status,
      id: createdPaymentOrder.id,
    };
  }
}
