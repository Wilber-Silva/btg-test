import { GetOnePaymentOrderUsecase } from './get-one-payment-order.usecase';
import { SettleTheTransferUsecase } from './settle-transfer.usecase';

export const usecases = [SettleTheTransferUsecase, GetOnePaymentOrderUsecase];
