import { DatabaseModule } from '@Database/database.module';
import { Module } from '@nestjs/common';
import { PaymentUsecaseModule } from './Payment/payment-usecase.module';
@Module({
  imports: [DatabaseModule, PaymentUsecaseModule],
})
export class UseCasesModule {}
