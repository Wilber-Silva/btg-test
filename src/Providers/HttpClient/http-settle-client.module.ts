import { Module } from '@nestjs/common';
import { HttpSettleClientService } from './http-settle-client.service';

@Module({
  providers: [HttpSettleClientService],
  exports: [HttpSettleClientService],
})
export class HttpSettleClientModule {}
