import { PaymentOrderStatus } from '@Enum';
import { Injectable } from '@nestjs/common';
import { IPaymentOrderSchema } from '@Protocols/Schema';

@Injectable()
export class HttpSettleClientService {
  async sendSettle(params: IPaymentOrderSchema): Promise<PaymentOrderStatus> {
    return PaymentOrderStatus.CREATED;
  }
}
