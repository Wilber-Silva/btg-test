import { DatabaseModule } from '@Database/database.module';
import { Module } from '@nestjs/common';
import { HttpSettleClientModule } from '@Providers/HttpClient/http-settle-client.module';
import { PaymentOrderUsecaseModule } from '@UseCases/Payment/Orders/order-usecase.module';
import { GetOnePaymentOrderUsecase } from '@UseCases/Payment/Orders/usecases/get-one-payment-order.usecase';
import { SettleTheTransferUsecase } from '@UseCases/Payment/Orders/usecases/settle-transfer.usecase';
import { PaymentOrderController } from './orders.controller';
import { PaymentOrderService } from './orders.service';

@Module({
  imports: [PaymentOrderUsecaseModule, HttpSettleClientModule, DatabaseModule],
  controllers: [PaymentOrderController],
  providers: [
    PaymentOrderService,
    SettleTheTransferUsecase,
    GetOnePaymentOrderUsecase,
  ],
})
export class PaymentOrderModule {}
