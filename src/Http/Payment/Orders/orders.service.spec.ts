import { Test, TestingModule } from '@nestjs/testing';
import faker from 'faker';
import { v4 as generateId } from 'uuid';
import { IHttpRequestPaymentOrdersSettleTransfer } from '@Protocols/HttpRequest';
import { PaymentOrderController } from './orders.controller';
import { PaymentOrderService } from './orders.service';
import { PaymentOrderUsecaseModule } from '@UseCases/Payment/Orders/order-usecase.module';
import { SettleTheTransferUsecase } from '@UseCases/Payment/Orders/usecases/settle-transfer.usecase';
import { PaymentOrderStatus } from '@Enum';

describe('AuthController', () => {
  let paymentOrderService: PaymentOrderService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [PaymentOrderUsecaseModule],
      controllers: [PaymentOrderController],
      providers: [PaymentOrderService, SettleTheTransferUsecase],
    }).compile();

    paymentOrderService = module.get<PaymentOrderService>(PaymentOrderService);
  });

  describe('root', () => {
    it('should settle the transfer', async () => {
      const mockedResult = PaymentOrderStatus.CREATED;
      const serviceMethodSpy = jest
        .spyOn(SettleTheTransferUsecase.prototype as any, 'execute')
        .mockImplementation(() => {
          return Promise.resolve(mockedResult);
        });
      const dto: IHttpRequestPaymentOrdersSettleTransfer = {
        externalId: generateId(),
        amount: Number(faker.finance.amount()),
        expectedOn: '2020-01-01',
      };
      const result = await paymentOrderService.settleTheTransfer(dto);

      expect(result.internalId).toBeDefined();
      expect(result.status).toEqual(mockedResult);

      expect(serviceMethodSpy).toHaveBeenCalledWith(dto);
      expect(serviceMethodSpy).toHaveBeenCalledTimes(1);
    });
  });
});
