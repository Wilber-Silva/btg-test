import { PaymentOrderOperation } from '@Enum';
import { Inject } from '@nestjs/common';
import { IHttpResponsePaymentOrdersSettleTransfer } from '@Protocols/HttpResponse';
import { IPaymentOrderModel } from '@Protocols/Model';
import {
  IGetOnePaymentOrderUseCase,
  ISettleTheTransferUsecase,
} from '@Protocols/Usecase';
import { GetOnePaymentOrderUsecase } from '@UseCases/Payment/Orders/usecases/get-one-payment-order.usecase';
import { SettleTheTransferUsecase } from '@UseCases/Payment/Orders/usecases/settle-transfer.usecase';
import { SettleTheTransferRequestDto } from './DataObjectTransfer';

export class PaymentOrderService {
  constructor(
    @Inject(SettleTheTransferUsecase)
    private readonly settleTheTransferUsecase: ISettleTheTransferUsecase,
    @Inject(GetOnePaymentOrderUsecase)
    private readonly getOnePaymentOrderUseCase: IGetOnePaymentOrderUseCase,
  ) {}
  async settleTheTransfer(
    settleTheTransferRequestDto: SettleTheTransferRequestDto,
  ): Promise<IHttpResponsePaymentOrdersSettleTransfer> {
    const settleTheTransferResult = await this.settleTheTransferUsecase.execute(
      {
        operation: PaymentOrderOperation.TRANSFER,
        paymentOrdersSettleTransfer: settleTheTransferRequestDto,
      },
    );
    return {
      internalId: settleTheTransferResult.id,
      status: settleTheTransferResult.status,
    };
  }

  async getOneById(internalId: string): Promise<IPaymentOrderModel> {
    return this.getOnePaymentOrderUseCase.execute(internalId);
  }
}
