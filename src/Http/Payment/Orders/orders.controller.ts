import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Inject,
  Param,
  Post,
} from '@nestjs/common';
import {
  ApiBody,
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

import {
  GetOnePaymentOrderDto,
  SettleTheTransferRequestDto,
} from './DataObjectTransfer';
import { IHttpResponsePaymentOrdersSettleTransfer } from '@Protocols/HttpResponse';
import { PaymentOrderService } from './orders.service';
@ApiTags('payment/orders')
@Controller('payment/orders')
export class PaymentOrderController {
  constructor(
    @Inject(PaymentOrderService)
    private readonly paymentOrderService: PaymentOrderService,
  ) {}

  @Post()
  @ApiOperation({
    description: 'Settle the transfer.',
    tags: ['transfer', 'post', 'payment', 'payment/orders'],
  })
  @ApiHeader({ name: 'X-API-KEY', required: true })
  @ApiBody({ type: SettleTheTransferRequestDto })
  @ApiResponse({ status: HttpStatus.OK, type: SettleTheTransferRequestDto })
  async settleTheTransfer(
    @Body() settleTheTransferRequestDto: SettleTheTransferRequestDto,
  ): Promise<IHttpResponsePaymentOrdersSettleTransfer> {
    return this.paymentOrderService.settleTheTransfer(
      settleTheTransferRequestDto,
    );
  }

  @Get(':internalId')
  @ApiOperation({
    description: 'Settle the transfer.',
    tags: ['transfer', 'post', 'payment', 'payment/orders'],
  })
  @ApiHeader({ name: 'X-API-KEY', required: true })
  @ApiBody({ type: SettleTheTransferRequestDto })
  @ApiResponse({ status: HttpStatus.OK, type: SettleTheTransferRequestDto })
  async getOneById(@Param() params: GetOnePaymentOrderDto): Promise<any> {
    return this.paymentOrderService.getOneById(params.internalId);
  }
}
