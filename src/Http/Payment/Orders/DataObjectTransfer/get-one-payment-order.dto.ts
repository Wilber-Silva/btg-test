import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';

export class GetOnePaymentOrderDto {
  @ApiProperty({ type: String, format: 'uuid', required: true })
  @IsNotEmpty({ message: 'The id is required' })
  @IsUUID('4', { message: 'Invalid UUID' })
  internalId: string;
}
