import { ApiProperty } from '@nestjs/swagger';
import { IHttpRequestPaymentOrdersSettleTransfer } from '@Protocols/HttpRequest';
import {
  IsDateString,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsUUID,
} from 'class-validator';

export class SettleTheTransferRequestDto
  implements IHttpRequestPaymentOrdersSettleTransfer
{
  @ApiProperty({ type: String, format: 'uuid', required: true })
  @IsNotEmpty({ message: 'The id is required' })
  @IsUUID('4', { message: 'Invalid UUID' })
  externalId: string;

  @ApiProperty({ type: Number, required: true })
  @IsNumber(
    { allowNaN: false, allowInfinity: false },
    { message: 'Amount must be a number' },
  )
  amount: number;

  @ApiProperty({ type: String, format: 'date', required: true })
  @IsNotEmpty({ message: 'The expected on is required' })
  @IsDateString(undefined, { message: 'The expected on must be a date' })
  expectedOn: string | Date;

  @ApiProperty({ type: String, format: 'uuid', required: false })
  @IsOptional()
  @IsDateString(undefined, { message: 'The expected on must be a date' })
  dueDate?: Date | string;
}
