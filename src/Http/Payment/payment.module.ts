import { Module } from '@nestjs/common';
import { UseCasesModule } from '@UseCases/usercases.module';
import { PaymentOrderModule } from './Orders/orders.module';

@Module({
  imports: [PaymentOrderModule, UseCasesModule],
  controllers: [],
  providers: [],
})
export class PaymentModule {}
